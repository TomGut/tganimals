package pl.codementors.animals.Model;

public class Dog extends Animal implements HomePet{

    public Dog(){

    }

    public Dog(String name, int age) {
        super(name, age);
    }

    public void hau(){
        System.out.println("HAU");
    }

    public void pet(){
        hau();
    }

}
