package pl.codementors.animals;

import pl.codementors.animals.Model.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        Animal[] animals = new Animal[10];

        //Polimorfizm - tworzymy pusty obiekt do wykorzystywania w pętli case 5 podczas rzutowania
        Animal animally = null;

        System.out.println("ZWIERZAKI\n");

        boolean running = true;

        while(running){

            System.out.print("POLECENIA\n 0 - zakończ program\n 1 - stwórz zwierzę\n 2 - wypisz wybrane zwierzę\n " +
                    "3 - wypisz wszystkie zwierzeta\n 4 - wszystkie zwierzęta wydają głos\n 5 - głaskanie zwierząt\n " +
                    "6 - głaskanie konkretnego zwierzaka\n");

            int numberForSwitch = scanner.nextInt();

            switch(numberForSwitch){
                //zakończenie programu
                case 0: {

                    System.out.println("Wychodzę z programu");
                    running = false;

                    break;

                }
                //tworzy zwierzę
                case 1: {

                    System.out.println("Podaj typ zwierzątka C-cat, D-dog, W-woolf");
                    String animalType = scanner.next();

                    System.out.println("Podaj Imię zwierzątka");
                    String animalName = scanner.next();

                    System.out.println("Podaj wiek zwierzątka");
                    int animalAge = scanner.nextInt();

                    if("C".equals(animalType)){

                        System.out.println("Wybrałeś cat");

                        //Polimorfizm
                        animally = new Cat(animalName, animalAge);

                    }else if("D".equals(animalType)){

                        System.out.println("Wybrałeś dog");

                        animally = new Dog(animalName, animalAge);

                    }else if("W".equals(animalType)){

                        System.out.println("Wybrałeś woolf");

                        animally = new Woolf(animalName, animalAge);

                    }else{
                        System.out.println("Podałeś złą literę");
                        break;
                    }

                    System.out.println("Do której klatki dodać zwierzę ?");

                    int cageIndex = scanner.nextInt();
                    //Praktyczne zastosowanie polimorfizmu - przypisanie różnego typu zwierżęcia do tablicy animals
                    animals[cageIndex] = animally;

                    break;
                }
                //wypisuje konkretne zwierzę
                case 2: {

                    System.out.println("Z której klatki wypisać zwierzę ?");

                    int animalIndex = scanner.nextInt();

                    System.out.println("Wypisuję zwierzę z klatki o indeksie nr. " + animalIndex);
                    System.out.println("Twoję zwierzę jest typu " + animals[animalIndex].getClass().getSimpleName());
                    System.out.println("Imię :" + animals[animalIndex].getName());
                    System.out.println("Wiek : " + animals[animalIndex].getAge());

                    break;
                }
                //wypisuje wszystkie zwierzęta
                case 3: {

                    System.out.println("Wypisuję wszystkie zwierzęta z tablicy");
                    System.out.println();

                        for(int i=0; i < animals.length; i++){

                            if(animals[i] != null){

                                System.out.println("Indeks klatki nr. " + i);
                                System.out.println("Zwierze jest typu: " + animals[i].getClass().getSimpleName());
                                System.out.println("Imię :" + animals[i].getName());
                                System.out.println("Wiek :" + animals[i].getAge() + " \n");

                            }
                        }

                    break;
                }
                //wszystkie zwierzęta wydają głos
                case 4:{
                    //iterujemy po "długości" tablicy
                    for(int i=0; i < animals.length; i++){
                        //spr czy indeks nie jest pusty jak pusty pomija i idzie do pełnego

                        if(animals[i] != null){
                            //spr. czy dana klasa jest instancją nadklasy
                            
                            if(animals[i] instanceof Woolf){

                                //Rzutowanie
                                ((Woolf) animals[i]).woof();
                                System.out.println("Wilk o imieniu " + animals[i].getName());
                                System.out.println();

                            }else if(animals[i] instanceof Dog){

                                //Rzutowanie
                                ((Dog) animals[i]).hau();
                                System.out.println();
                                System.out.println("Pies o imieniu " + animals[i].getName());
                                System.out.println();

                            }else if(animals[i] instanceof Cat){

                                //Rzutowanie
                                ((Cat) animals[i]).miaow();
                                System.out.println("Kot o imieniu " + animals[i].getName());
                                System.out.println();

                            }else{

                                System.out.println("To zwierze nie wydaje dźwięków");

                            }
                        }
                    }

                    break;
                }
                //głaskanie wszystkich zwierząt
                case 5: {

                    for(int i=0; i < animals.length; i++) {
                        //spr czy indeks nie jest pusty jak pusty pomija i idzie do pełnego
                        if (animals[i] != null) {
                            //spr. czy dana klasa jest instancją nadklasy - wtym wypadku odwołujemy się do Interfejsu
                            if (animals[i] instanceof HomePet) {
                                System.out.println();
                                System.out.println("Głaskasz zwierzę o imieniu " + animals[i].getName() + " a ono wydaje dźwięk ");
                                ((HomePet) animals[i]).pet();
                                System.out.println();
                            }
                        }
                    }

                    break;
                }
                //głaskanie konkretnego zwierzątka
                case 6: {

                    System.out.println("Które zwierzę głaskamy - podaj indeks w klatce");

                    int cageIndex = scanner.nextInt();

                        if(animals[cageIndex] != null){

                            if (animals[cageIndex] instanceof HomePet) {

                                System.out.println();
                                System.out.println("Głaskasz zwierzę o imieniu " + animals[cageIndex].getName() + " a ono wydaje dźwięk ");
                                ((HomePet) animals[cageIndex]).pet();
                                System.out.println();

                            }
                    }

                    break;
                }
            }
        }
    }
}
