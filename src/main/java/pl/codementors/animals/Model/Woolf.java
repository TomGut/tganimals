package pl.codementors.animals.Model;

public class Woolf extends Animal implements HomePet{

    public Woolf(){

    }

    public Woolf(String name, int age) {
        super(name, age);
    }

    public void woof(){
        System.out.println("WOOF");
    }

    public void pet() {
        woof();
    }
}
