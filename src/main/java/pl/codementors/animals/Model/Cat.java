package pl.codementors.animals.Model;

public class Cat extends Animal implements HomePet{

    public Cat(){

    }

    public Cat(String name, int age) {
        super(name, age);
    }

    public void miaow(){
        System.out.println("MIAOW");
    }

    public void pet(){
        miaow();
    }

}
